"""
Classify cosmic ray mass numbers from toy air shower footprints.
Run this script with 'pygpu %file' in the code editor or terminal.
"""
import numpy as np
import matplotlib.pyplot as plt
import os
import sys
import tensorflow as tf
from tensorflow import keras
models = keras.models
layers = keras.layers

# ---------------------------------------------------------
# Boilerplate. You can ignore this part.
# ---------------------------------------------------------
try:
    CONDOR_ID = os.environ['CONDOR_ID']
except KeyError:
    sys.exit('Error: Run this script with "pygpu %file"')
tf.logging.set_verbosity(tf.logging.ERROR)

folder = 'train-mass-%s' % CONDOR_ID  # folder for training results
os.makedirs(folder)


# ---------------------------------------------------------
# Load and preprocess toy data
# ---------------------------------------------------------
data = np.load('/net/scratch/deeplearning/airshower/data.npz')

muons = data['T']  # arrays of positions (x, y) and times t
mass = data['A']  # mass number A
species = data['species']  # simulated mass numbers (1, 4, 14, 56)


def muonImage(muons, xbins, tbins=None):
    """ Create histogram of muon hits for a single event """
    x, y, t = muons
    if tbins is None:
        # 2D histogram of spatial distribution
        return np.histogram2d(x, y, bins=(xbins, xbins))[0]
    else:
        # 3D histogram of spatial + time distribution
        return np.histogramdd((x, y, t), bins=(xbins, xbins, tbins))[0]


# Create 10x10 pixel image of total number of muon hits, no time dimension
npix = 10
xbins = np.linspace(-5000, 5000, npix + 1)  # pixel borders in [m]
image = np.array([muonImage(m, xbins) for m in muons])

# normalize images to hide total number of muons
image /= np.sum(image, axis=(1, 2))[:, None, None]

# reshape for neural network (add axis of size 1)
image = image.reshape(-1, npix, npix, 1)
mass_onehot = np.array([mass == a for a in species], dtype=float).T

# split train, validation and test samples
i0, i1, i2, i3 = 0, 30000, 34000, 40000
X_train, Y_train = image[i0:i1], mass_onehot[i0:i1]
X_valid, Y_valid = image[i1:i2], mass_onehot[i1:i2]
X_test,  Y_test = image[i2:i3], mass_onehot[i2:i3]


# ----------------------------------------------------------
# Define model
# ----------------------------------------------------------
model = models.Sequential([
    layers.Conv2D(32, (3, 3), padding='valid', activation='relu', input_shape=(npix, npix, 1)),
    layers.Conv2D(32, (3, 3), padding='valid', activation='relu'),
    layers.Conv2D(64, (3, 3), padding='valid', activation='relu'),
    layers.Flatten(),
    layers.Dense(128, activation='relu'),
    layers.Dropout(0.5),
    layers.Dense(4, activation='softmax')])

print(model.summary())


# ----------------------------------------------------------
# Training
# ----------------------------------------------------------
model.compile(
    loss='categorical_crossentropy',
    optimizer=keras.optimizers.Adam(lr=1E-3),
    metrics=['accuracy'])

batch_size = 32
result = model.fit(X_train, Y_train,
                   batch_size=batch_size,
                   epochs=75,
                   verbose=1,
                   validation_data=(X_valid, Y_valid),
                   callbacks=[keras.callbacks.CSVLogger(folder + '/history.csv')])

model.save(folder+'/model.h5')


# ----------------------------------------------------------
# Evaluation and Plots
# ----------------------------------------------------------
print('Model performance (loss, accuracy)')
print('Train: %.4f, %.4f' % tuple(model.evaluate(X_train, Y_train, verbose=0, batch_size=batch_size)))
print('Valid: %.4f, %.4f' % tuple(model.evaluate(X_valid, Y_valid, verbose=0, batch_size=batch_size)))
print('Test:  %.4f, %.4f' % tuple(model.evaluate(X_test,  Y_test,  verbose=0, batch_size=batch_size)))


# training curves
history = np.genfromtxt(folder+'/history.csv', delimiter=',', names=True)

fig, ax = plt.subplots(1)
ax.plot(history['epoch'], history['loss'],     label='training')
ax.plot(history['epoch'], history['val_loss'], label='validation')
ax.legend()
ax.set(xlabel='epoch', ylabel='loss')
fig.savefig(folder+'/loss.png')

fig, ax = plt.subplots(1)
ax.plot(history['epoch'], history['acc'],     label='training')
ax.plot(history['epoch'], history['val_acc'], label='validation')
ax.legend()
ax.set(xlabel='epoch', ylabel='accuracy')
fig.savefig(folder+'/accuracy.png')


# confusion matrix
Y_predict = model.predict(X_test, batch_size=128)
Y_predict = np.argmax(Y_predict, axis=1)
Y_true = np.argmax(Y_test, axis=1)

C = np.histogram2d(Y_true, Y_predict, bins=np.linspace(-0.5, 3.5, 5))[0]
Cn = C / np.sum(C, axis=1)
fig = plt.figure()
plt.imshow(Cn, interpolation='nearest', vmin=0, vmax=1, cmap=plt.cm.YlGnBu)
plt.colorbar()
plt.xlabel('predicted mass number')
plt.ylabel('true mass number')
plt.xticks(range(4), species)
plt.yticks(range(4), species)
for x in range(4):
    for y in range(4):
        plt.annotate('%i' % C[x, y], xy=(y, x), ha='center', va='center')
fig.savefig(folder+'/confusion.png')

keras.backend.clear_session()
