"""
Reconstruct Xmax from toy air shower footprints (regression task).
Run this script with 'pygpu %file' in the code editor or terminal.
"""
import numpy as np
import matplotlib.pyplot as plt
import os
import sys
import tensorflow as tf
from tensorflow import keras
models = keras.models
layers = keras.layers

# ---------------------------------------------------------
# Boilerplate. You can ignore this part.
# ---------------------------------------------------------
try:
    CONDOR_ID = os.environ['CONDOR_ID']
except KeyError:
    sys.exit('Error: Run this script with "pygpu %file"')
tf.logging.set_verbosity(tf.logging.ERROR)

folder = 'train-Xmax-%s' % CONDOR_ID  # folder for training results
os.makedirs(folder)


# ---------------------------------------------------------
# Load and preprocess toy data
# ---------------------------------------------------------
data = np.load('/net/scratch/deeplearning/airshower/data.npz')

muons = data['T']  # arrays of positions (x, y) and times t
xmax = data['X']  # Xmax [g/cm^2]
species = data['species']  # simulated mass numbers (1, 4, 14, 56)


def muonImage(muons, xbins, tbins=None):
    """ Create histogram of muon hits for a single event """
    x, y, t = muons
    if tbins is None:
        # 2D histogram of spatial distribution
        return np.histogram2d(x, y, bins=(xbins, xbins))[0]
    else:
        # 3D histogram of spatial + time distribution
        return np.histogramdd((x, y, t), bins=(xbins, xbins, tbins))[0]


# Create 10x10 pixel image of total number of muon hits, no time dimension
npix = 10
xbins = np.linspace(-5000, 5000, npix + 1)  # pixel borders in [m]
image = np.array([muonImage(m, xbins) for m in muons])

# normalize images to hide total number of muons
image /= np.sum(image, axis=(1, 2))[:, np.newaxis, np.newaxis]

# reshape for neural network (add axis of size 1)
image = image.reshape(-1, npix, npix, 1)
xmax = xmax.reshape(-1, 1)

# split train, validation and test samples
i0, i1, i2, i3 = 0, 30000, 35000, 40000
X_train, Y_train = image[i0:i1], xmax[i0:i1]
X_valid, Y_valid = image[i1:i2], xmax[i1:i2]
X_test,  Y_test = image[i2:i3], xmax[i2:i3]


# ----------------------------------------------------------
# Define model
# ----------------------------------------------------------
model = models.Sequential([
    layers.Conv2D(32, (3, 3), padding='valid', activation='relu', input_shape=(npix, npix, 1)),
    layers.Conv2D(32, (3, 3), padding='valid', activation='relu'),
    layers.Conv2D(64, (3, 3), padding='valid', activation='relu'),
    layers.Flatten(),
    layers.Dense(256, activation='relu'),
    layers.Dropout(0.3),
    layers.Dense(1)])

print(model.summary())


# ----------------------------------------------------------
# Training
# ----------------------------------------------------------
model.compile(
    loss='mean_squared_error',
    optimizer=keras.optimizers.Adam(lr=1E-3),
    metrics=['mean_absolute_error'])

batch_size = 128
result = model.fit(X_train, Y_train,
                   batch_size=batch_size,
                   epochs=25,
                   verbose=1,
                   validation_data=(X_valid, Y_valid),
                   callbacks=[keras.callbacks.CSVLogger(folder + '/history.csv')])

model.save(folder+'/model.h5')


# ----------------------------------------------------------
# Evaluation and Plots
# ----------------------------------------------------------
print('Model performance (loss, mean absolute error)')
print('Train: %.4f, %.4f' % tuple(model.evaluate(X_train, Y_train, verbose=0, batch_size=batch_size)))
print('Valid: %.4f, %.4f' % tuple(model.evaluate(X_valid, Y_valid, verbose=0, batch_size=batch_size)))
print('Test:  %.4f, %.4f' % tuple(model.evaluate(X_test,  Y_test,  verbose=0, batch_size=batch_size)))


# training curves
history = np.genfromtxt(folder+'/history.csv', delimiter=',', names=True)

fig, ax = plt.subplots(1)
ax.plot(history['epoch'], history['loss'],     label='training')
ax.plot(history['epoch'], history['val_loss'], label='validation')
ax.set_yscale("log", nonposy='clip')
ax.legend()
ax.set(xlabel='epoch', ylabel='loss')
fig.savefig(folder+'/loss.png')

fig, ax = plt.subplots(1)
ax.plot(history['epoch'], history['mean_absolute_error'],     label='training')
ax.plot(history['epoch'], history['val_mean_absolute_error'], label='validation')
ax.legend()
ax.set(xlabel='epoch', ylabel='accuracy')
fig.savefig(folder+'/mean_absolute_error.png')


# predict Xmax
Xmax_train = model.predict(X_train, verbose=0, batch_size=batch_size)
Xmax_valid = model.predict(X_valid, verbose=0, batch_size=batch_size)
Xmax_test = model.predict(X_test,  verbose=0, batch_size=batch_size)

# difference to true Xmax
dXmax_train = Xmax_train - Y_train
dXmax_valid = Xmax_valid - Y_valid
dXmax_test = Xmax_test - Y_test

mean_train, std_train = np.mean(dXmax_train), np.std(dXmax_train)
mean_valid, std_valid = np.mean(dXmax_valid), np.std(dXmax_valid)
mean_test, std_test = np.mean(dXmax_test), np.std(dXmax_test)

print('Model performance (dX = predicted - true Xmax)')
print('Train: <dX>=%.2f, std(dX)=%.2f' % (mean_train, std_train))
print('Valid: <dX>=%.2f, std(dX)=%.2f' % (mean_valid, std_valid))
print('Test:  <dX>=%.2f, std(dX)=%.2f' % (mean_test, std_test))


# scatter
fig, ax = plt.subplots(1)
label = '%s\n$\mu=%.1f, \sigma=%.1f$'
ax.scatter(Y_train, Xmax_train, label=label % ('training set', mean_train, std_train))
ax.scatter(Y_valid, Xmax_valid, label=label % ('validation set', mean_valid, std_valid))
ax.scatter(Y_test,  Xmax_test,  label=label % ('test set', mean_test, std_test))
ax.plot((600, 1000), (600, 1000), 'k--')
ax.legend()
ax.set_xlabel('true $X_\mathrm{max}$ [g/cm$^2$]')
ax.set_ylabel('predicted $X_\mathrm{max}$ [g/cm$^2$]')
ax.grid()
ax.set_aspect('equal')
fig.savefig(folder+'/scatter.png')

keras.backend.clear_session()
